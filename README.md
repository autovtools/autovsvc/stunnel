# stunnel

Containerized `stunnel` proxy that can attach to existing services, transparently providing secure TLS support.
