#!/bin/sh
CONFIG_TEMPLATE="/run/stunnel/stunnel.conf.template"
CONFIG="/run/stunnel/stunnel.conf"

eval "echo \"$(cat "${CONFIG_TEMPLATE}")\"" > "${CONFIG}"
chmod 0400 "${CONFIG}"
stunnel "${CONFIG}"
